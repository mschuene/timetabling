# Automatische Stundenplanerzeugung mittels Weighted Partial MAX-SAT

Die elektronische Form der Arbeit befindet sich in der Datei
Automatische Stundenplanerzeugung mittels Weighted Partial MAX-SAT.

Die Datei wurde aus dem literate program in der Datei timetabling.org
erzeugt. Das Programm wurde in den Unterordner src geschrieben. Es ist
in clojure geschrieben und man kann mit den üblichen tools eine REPL
starten und das Programm damit ausprobieren. Eine gute Anleitung zur
Installation der nötigen Tools ist unter
http://www.braveclojure.com/getting-started/ zu finden.

Das Repository des Projektes ist unter
https://bitbucket.org/mschuene/timetabling zu finden.







